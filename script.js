moment.locale('ru');
$(function(){
    $("#amount, #date").on("keydown", function(event){
        if (event.keyCode == 46 || event.keyCode == 8) {
            return;
        } else if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    });
    $("#amount").on("change", function(){
        var amount = Number($(this).val());
        
        if(amount < 1000)
            $(this).val(1000);
            
        if(amount > 35000)
            $(this).val(35000);
            
        if(Number($("#amount").val()) % 1000 != 0){
            var number = Number($("#amount").val());
            $(this).val(number + 1000 - number%1000);
        }
    });
    $("#date").on("change", function(){
        var date = $(this).val();
        if(date<5)
            $(this).val(5);
        
        if(date > 45)
            $(this).val(45);

        $("#refund-date").html(moment().add($(this).val(), 'days').format('LL'));
    });
    $("#date, #amount").on("change", function(){
        console.log($("#date").val());
        if($("#date").val() != '' && $("#amount").val() != '') {
            var refund = Number($("#amount").val()) + Number($("#amount").val()) * Number($("#date").val()) * 0.000333;
            console.log(refund);
            refund = Math.floor(refund * Math.pow(10, 2)) / Math.pow(10, 2);
            $("#refund").html(refund + 'р');
        }
    });
});